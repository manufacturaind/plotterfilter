#!/usr/bin/env python3
from bs4 import BeautifulSoup
import sys

infile = sys.argv[1]

with open(infile) as fp:
    svg = BeautifulSoup(fp, 'xml')

# remove clipping paths with empty rects
for node in svg.find_all('clipPath'):
    if node.find('rect'):
        r = node.find('rect')
        if list(r.attrs.keys()) == ['x', 'y', 'width', 'height']:
            node.extract()
# remove references to clipping paths
for node in svg.find_all(attrs={'clip-path': True}):
    node.attrs.pop('clip-path')

# groups with single paths? replace by the path itself
# singlegroups = [g for g in svg.find_all('g') if len(g.find_all('path') == 1)]
singlegroups = [g for g in svg.find_all('g')]
for node in singlegroups:
    if len(node.find_all('path')) == 1:
        path = node.find('path')
        path.attrs['id'] = node.attrs['id']
        node.unwrap()

# remove the group with <use> tags (clones) and take the paths out of the <defs> tag
clonegroup = [g for g in svg.find_all('g') if g.find('use')].pop()
clonegroup.extract()
svg.find('defs').unwrap()

print(svg.prettify())
